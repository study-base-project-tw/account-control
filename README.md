# Account Control

A ideia é criar um projeto do zero e praticar os seguintes pontos:
- Spring Boot / Web 
- TDD
- Clean Code
- Pareamento

## Histórias

As histórias do projeto terão cenários descritos como features do [Cucumber](https://cucumber.io/docs/guides/10-minute-tutorial/) 
mas estarão como Issues também no Gitlab


## Ideias

[lucas.zingano]

A minha ideia é a de fazermos algumas versões do projeto, a saber:
1. Features escritas chamando direto os objetos de negócio.
2. Reescrita de Features chamando API v1, usando Spring Boot MVC c/ testes unitários do JUnit.
3. Configuração pipeline ci/docker
4. Reescrita de Features chamando API v2 e usando WebFlux c/ testes unitários do JUnit.

Futuro/possiblidades: Quebrar serviços, config server, trocar/adicionar persistência SQL ou NoSQL/event sourcing architecture.
Por favor, colaborem se estiverem interessados em usar algo a mais.