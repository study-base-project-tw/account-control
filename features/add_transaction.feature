Feature: Add Transaction
    As an Account Holder
    I want to add transactions to my account
    In order to manage my budget in a more effective way

    Scenario: Adding Expense
        Given that I have an Account
        When I add an expense
        Then my account should be updated with the corresponding expense

    Scenario: Adding Income
        Given that I have an account
        When I add an income
        Then my account should be updated with the corresponding income
