Feature: View Timeline
    As an Account Holder
    I want to view my timeline
    In order to review my transactions

    Scenario: No transactions
        Given that I have an Account
        And that I have no transactions
        When I access my timeline
        Then I should see a message saying "Your account is empty! :("

    Scenario: Incomes & Expenses
        Given that I have an Account
        And that I have the following expenses transactions:
            | description    | amount |
            | Fuel           | 20.00  |
            | Food           | 30.00  |
            | Rent           | 40.00  |
        Given that I have the following incomes transactions:
            | description    | amount |
            | Fuel           | 20.00  |
            | Food           | 30.00  |
            | Rent           | 40.00  |
        When I access my timeline
        Then I should see a list of transactions