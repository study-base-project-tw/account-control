package integration;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import accountcontrol.models.Account;

import static org.junit.Assert.*;

public class Stepdefs {

    private Account acc;
    private int balance;

    @Given("^that I have an Account$")
    public void thatIHaveAnAccount() {
        acc = new Account();
    }

    @Given("^that I have no transactions$")
    public void thatIHaveNoTransactions() {
        assertEquals(0, acc.getTransactions().size());
    }

    @When("^I check my balance$")
    public void iCheckMyBalance() {
        balance = acc.getBalance();
    }

    @Then("^I should see my balance as \"([^\"]*)\"$")
    public void iShouldSeeMyBalanceAs(String arg1) {
        assertEquals(0, balance);
    }

    @Given("that I have the following expense (\\\\d+.\\\\d+)$")
    public void thatIHaveTheFollowingExpense(double arg1) {
        acc.addTransaction(arg1);
    }
}
