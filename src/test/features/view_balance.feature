Feature: View Balance
    As an Account Holder
    I want to check my balance
    In order to know how much money do I have left

    Scenario: No transactions
        Given that I have an Account
        And that I have no transactions
        When I check my balance
        Then I should see my balance as "0"

    Scenario: Only Expenses
        Given that I have an Account
        And that I have the following expense "-40.00"
#        And that I have the following expenses transactions:
#            | description    | amount |
#            | Fuel           | 20.00  |
#            | Food           | 30.00  |
#            | Rent           | 40.00  |
        When I check my balance
        Then I should see my balance as "-40.00"

#    Scenario: Only Incomes
#        Given that I have an Account
#        And that I have the following incomes transactions:
#            | description    | amount |
#            | Fuel           | 20.00  |
#            | Food           | 30.00  |
#            | Rent           | 40.00  |
#        When I check my balance
#        Then I should see my balance as "90.00"
